// In the following line, you should include the prefixes of implementations you want to test.
window.indexedDB =
  window.indexedDB ||
  window.mozIndexedDB ||
  window.webkitIndexedDB ||
  window.msIndexedDB;
// DON'T use "var indexedDB = ..." if you're not in a function.
// Moreover, you may need references to some window.IDB* objects:
window.IDBTransaction = window.IDBTransaction ||
  window.webkitIDBTransaction ||
  window.msIDBTransaction || {READ_WRITE: 'readwrite'}; // This line should only be needed if it is needed to support the object's constants for older browsers
window.IDBKeyRange =
  window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
// (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
if (!window.indexedDB) {
  window.alert(
      'Your browser doesn\'t support a stable version of IndexedDB. Such and such feature will not be available.'
  );
}

// This is what our customer data looks like.
const customerData = [
  {ssn: '444-44-4444', name: 'Bill', age: 35, email: 'bill@company.com'},
  {ssn: '555-55-5555', name: 'Donna', age: 32, email: 'donna@home.org'},
];

let db;

const createDB = () => {
  // Let us open our database
  const request = window.indexedDB.open('MyTestDatabase', 3);

  request.onerror = function(event) {
    // Do something with request.errorCode!
    console.log('openDB:error', event);
  };
  request.onupgradeneeded = function(event) {
    db = event.target.result;

    // Create an objectStore to hold information about our customers. We're
    // going to use "ssn" as our key path because it's guaranteed to be
    // unique - or at least that's what I was told during the kickoff meeting.
    const objectStore = db.createObjectStore('customers', {keyPath: 'ssn'});

    // Create an index to search customers by name. We may have duplicates
    // so we can't use a unique index.
    objectStore.createIndex('name', 'name', {unique: false});

    // Create an index to search customers by email. We want to ensure that
    // no two customers have the same email, so use a unique index.
    objectStore.createIndex('email', 'email', {unique: true});

    // Use transaction oncomplete to make sure the objectStore creation is
    // finished before adding data into it.
    objectStore.transaction.oncomplete = function(event) {
      // Store values in the newly created objectStore.
      const customerObjectStore = db
          .transaction('customers', 'readwrite')
          .objectStore('customers');
      customerData.forEach(function(customer) {
        customerObjectStore.add(customer);
      });
    };
  };
};
// event errors will get propoated. Request > Transaction > DB
// transaction types: readonly, readwrite, and versionchange

const getCustomerSSN = (ssn) => {
  const transaction = db.transaction(['customers']);
  const objectStore = transaction.objectStore('customers');
  const request = objectStore.get(ssn);
  request.onerror = function(event) {
    // Handle errors!
  };
  request.onsuccess = function(event) {
    // Do something with the request.result!
    alert('Name for SSN 444-44-4444 is ' + request.result.name);
  };
};

createDB();
setTimeout(() => {
  getCustomerSSN('444-44-4444');
}, 1000);
